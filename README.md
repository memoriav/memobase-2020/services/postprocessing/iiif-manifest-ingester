# IIIF Manifest Ingester

Service which loads the IIIF manifests into the MariaDB database.

## Configuration

* `BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap server addresses
* `CLIENT_ID`: Kafka client id
* `GROUP_ID`: Kafka consumer group id
* `TOPIC_IN`: Kafka topic where incoming topics are read from
* `TOPIC_PROCESS`: Kafka topic where status reports are written to
* `REPORTING_STEP_NAME`: The name of this service in reports.
* `MARIADB_HOST`: Host name of the MariaDB server
* `MARIADB_PORT`: Port of the MariaDB server
* `MARIADB_USER`: User of the MariaDB server
* `MARIADB_PASSWORD`: Password of the MariaDB server
* `MARIADB_DATABASE`: Respective database
* `MARIADB_TABLE`: Table where the manifests are stored
* `MARIADB_HOST_CERTS`: Path to MariaDB server certificates
* `SECURITY_PROTOCOL: Protocol used to communicate with brokers. Valid values are: `PLAINTEXT`, `SSL`, `SASL_PLAINTEXT`, `SASL_SSL`
* `SSL_KEYSTORE_TYPE: The file format of the key store file. This is optional for client. 
* `SSL_KEYSTORE_LOCATION: The location of the key store file. This is optional for client and can be used for two-way authentication for client
* `SSL_KEYSTORE_KEY`: Private key in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with PKCS#8 keys. If the key is encrypted, key password must be specified using `SSL_KEY_PASSWORD`
* `SSL_KEYSTORE_CERTIFICATE_CHAIN`: Certificate chain in the format specified by `SSL_KEYSTORE_TYPE`. Default SSL engine factory supports only PEM format with a list of X.509 certificates
* `SSL_TRUSTSTORE_TYPE: The file format of the trust store file. The values currently supported by the default `ssl.engine.factory.class` are `JKS`, `PKCS12` and `PEM`
* `SSL_TRUSTSTORE_LOCATION: The location of the trust store file
* `SSL_TRUSTSTORE_CERTIFICATES`: Trusted certificates in the format specified by `SSL_TRUSTSTORE_TYPE`. Default SSL engine factory supports only PEM format with X.509 certificates
* `SSL_KEY_PASSWORD`: The password of the private key in the Kafka key store file or the PEM key specified in `SSL_KEYSTORE_KEY`.

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Key store file in the format defined in `SSL_KEYSTORE_TYPE` and at the path defined in `SSL_KEYSTORE_LOCATION`
* Key store certificate chain in PEM format at the path defined in `SSL_KEYSTORE_CERTIFICATE_CHAIN`
* Key store key in PEM format at the path defined in `SSL_KEYSTORE_KEY`
* Trust store file in the format defined in `SSL_TRUSTSTORE_TYPE` and at the path defined in `SSL_TRUSTSTORE_LOCATION`
* Trust store certificates in PEM format at the path defined in `SSL_TRUSTSTORE_CERTIFICATES`

### Expected manifest table schema

```text
Field           Type            Key             Default             Note
---------------------------------------------------------------------------------------
creationtime    timestamp                       CURRENT_TIMESTAMP   Creation time
manifest        longtext                        NULL                Manifest content
sig             varchar(255)    PRI             NULL                Signature of record
```
