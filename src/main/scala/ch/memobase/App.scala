/*
 * IIIF Manifest Ingester
 * Writes IIIF manifests to database
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.logging.log4j.scala.Logging

object App
    extends Reporter
    with Logging
    with AppSettings
    with KConsumer
    with MariadbIndexer
    with scala.App {

  try {
    LazyList
      .continually(poll)
      .flatMap(records => {
        checkDBConnection(5)
        records.flatMap(record =>
          writeToDB(record.key, record.value, record.headers)
        ) ++
          writeRemainingRecordsToDB()
      })
      .foreach(res => reportResult(res))
  } catch {
    case e: Exception =>
      logger.error(e)
      sys.exit(1)
  } finally {
    logger.info("Shutting down application")
    consumerClose()
    dbConnectorClose().foreach(res => reportResult(res))
    closeReporter()
  }
}
