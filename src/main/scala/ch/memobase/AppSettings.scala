/*
 * IIIF Manifest Ingester
 * Writes IIIF manifests to database
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.Properties
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.config.SslConfigs
import org.apache.kafka.common.serialization.{
  StringDeserializer,
  StringSerializer
}
import org.apache.kafka.clients.CommonClientConfigs
import org.apache.logging.log4j.scala.Logging
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

trait AppSettings {
  self: Logging =>

  val kafkaClientId: String = sys.env("CLIENT_ID")
  val kafkaInputTopic: String = sys.env("TOPIC_IN")
  val kafkaReportingTopic: String = sys.env("TOPIC_PROCESS")
  val reportingStepName: String = sys.env("REPORTING_STEP_NAME")
  val mariaDBDSN: String = {
    val host: String = sys.env("MARIADB_HOST")
    val port: String = sys.env("MARIADB_PORT")
    val database: String = sys.env("MARIADB_DATABASE")
    val caCertPath: String = sys.env("MARIADB_HOST_CERTS")
    s"jdbc:mariadb://$host:$port/$database?sslMode=verify-full&serverSslCert=$caCertPath"
  }
  val mariaDBUser: String = sys.env("MARIADB_USER").replaceAll("\\s+$", "")
  val mariaDBPassword: String =
    sys.env("MARIADB_PASSWORD").replaceAll("\\s+$", "")
  val mariaDBTable: String = sys.env("MARIADB_TABLE")

  val consumerProps: Properties = {
    val properties = new Properties()
    properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
    properties.setProperty(
      ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG,
      "false"
    )
    properties.setProperty(ConsumerConfig.CLIENT_ID_CONFIG, kafkaClientId)
    properties.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
    properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, sys.env("GROUP_ID"))
    properties.setProperty(
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
      StringDeserializer().getClass.getName
    )
    properties.setProperty(
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
      StringDeserializer().getClass.getName
    )
    List(
      SslConfigs.SSL_KEYSTORE_TYPE_CONFIG,
      SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG,
      SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
      SslConfigs.SSL_KEY_PASSWORD_CONFIG,
      SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG,
      SslConfigs.SSL_KEYSTORE_KEY_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG,
      CommonClientConfigs.SECURITY_PROTOCOL_CONFIG,
      ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG,
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
      ConsumerConfig.CHECK_CRCS_CONFIG,
      ConsumerConfig.CLIENT_DNS_LOOKUP_CONFIG,
      ConsumerConfig.CLIENT_RACK_CONFIG,
      ConsumerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG,
      ConsumerConfig.DEFAULT_API_TIMEOUT_MS_CONFIG,
      ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,
      ConsumerConfig.ENABLE_METRICS_PUSH_CONFIG,
      ConsumerConfig.EXCLUDE_INTERNAL_TOPICS_CONFIG,
      ConsumerConfig.FETCH_MAX_BYTES_CONFIG,
      ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG,
      ConsumerConfig.FETCH_MIN_BYTES_CONFIG,
      ConsumerConfig.GROUP_INSTANCE_ID_CONFIG,
      ConsumerConfig.GROUP_PROTOCOL_CONFIG,
      ConsumerConfig.GROUP_REMOTE_ASSIGNOR_CONFIG,
      ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG,
      ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG,
      ConsumerConfig.ISOLATION_LEVEL_CONFIG,
      ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG,
      ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG,
      ConsumerConfig.MAX_POLL_RECORDS_CONFIG,
      ConsumerConfig.METADATA_MAX_AGE_CONFIG,
      ConsumerConfig.METRICS_NUM_SAMPLES_CONFIG,
      ConsumerConfig.METRICS_RECORDING_LEVEL_CONFIG,
      ConsumerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG,
      ConsumerConfig.METRIC_REPORTER_CLASSES_CONFIG,
      ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG,
      ConsumerConfig.RECEIVE_BUFFER_CONFIG,
      ConsumerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG,
      ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG,
      ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG,
      ConsumerConfig.RETRY_BACKOFF_MAX_MS_CONFIG,
      ConsumerConfig.RETRY_BACKOFF_MS_CONFIG,
      ConsumerConfig.SECURITY_PROVIDERS_CONFIG,
      ConsumerConfig.SEND_BUFFER_CONFIG,
      ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG,
      ConsumerConfig.SOCKET_CONNECTION_SETUP_TIMEOUT_MAX_MS_CONFIG,
      ConsumerConfig.SOCKET_CONNECTION_SETUP_TIMEOUT_MS_CONFIG
    )
      .map(p => (p, p.replaceAll(raw"\.", "_").toUpperCase))
      .collect {
        case p if sys.env.contains(p._2) => (p._1, p._2)
      }
      .foreach(p => {
        logger
          .debug(s"Setting Kafka consumer property ${p._1} -> ${sys.env(p._2)}")
        properties.setProperty(p._1, sys.env(p._2))
      })
    properties
  }
  val producerProps: Properties = {
    val properties = new Properties()
    properties.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, "16384")
    properties.setProperty(ProducerConfig.BUFFER_MEMORY_CONFIG, "33445532")
    properties.setProperty(ProducerConfig.CLIENT_ID_CONFIG, kafkaClientId)
    properties.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "zstd")
    properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")
    properties.setProperty(
      ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
      StringSerializer().getClass.getName
    )
    properties.setProperty(ProducerConfig.LINGER_MS_CONFIG, "1")
    properties.setProperty(
      ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
      StringSerializer().getClass.getName
    )
    List(
      SslConfigs.SSL_KEYSTORE_TYPE_CONFIG,
      SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG,
      SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
      SslConfigs.SSL_KEY_PASSWORD_CONFIG,
      SslConfigs.SSL_KEYSTORE_KEY_CONFIG,
      SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG,
      SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG,
      CommonClientConfigs.SECURITY_PROTOCOL_CONFIG,
      ProducerConfig.ACKS_CONFIG,
      ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
      ProducerConfig.CLIENT_DNS_LOOKUP_CONFIG,
      ProducerConfig.COMPRESSION_GZIP_LEVEL_CONFIG,
      ProducerConfig.COMPRESSION_LZ4_LEVEL_CONFIG,
      ProducerConfig.COMPRESSION_ZSTD_LEVEL_CONFIG,
      ProducerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG,
      ProducerConfig.DELIVERY_TIMEOUT_MS_CONFIG,
      ProducerConfig.ENABLE_METRICS_PUSH_CONFIG,
      ProducerConfig.INTERCEPTOR_CLASSES_CONFIG,
      ProducerConfig.MAX_BLOCK_MS_CONFIG,
      ProducerConfig.MAX_REQUEST_SIZE_CONFIG,
      ProducerConfig.METADATA_MAX_AGE_CONFIG,
      ProducerConfig.METADATA_MAX_IDLE_CONFIG,
      ProducerConfig.METRICS_NUM_SAMPLES_CONFIG,
      ProducerConfig.METRICS_RECORDING_LEVEL_CONFIG,
      ProducerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG,
      ProducerConfig.METRIC_REPORTER_CLASSES_CONFIG,
      ProducerConfig.PARTITIONER_ADPATIVE_PARTITIONING_ENABLE_CONFIG,
      ProducerConfig.PARTITIONER_AVAILABILITY_TIMEOUT_MS_CONFIG,
      ProducerConfig.PARTITIONER_CLASS_CONFIG,
      ProducerConfig.PARTITIONER_IGNORE_KEYS_CONFIG,
      ProducerConfig.RECEIVE_BUFFER_CONFIG,
      ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG,
      ProducerConfig.RECONNECT_BACKOFF_MS_CONFIG,
      ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG,
      ProducerConfig.RETRIES_CONFIG,
      ProducerConfig.RETRY_BACKOFF_MAX_MS_CONFIG,
      ProducerConfig.RETRY_BACKOFF_MS_CONFIG,
      ProducerConfig.SECURITY_PROVIDERS_CONFIG,
      ProducerConfig.SEND_BUFFER_CONFIG,
      ProducerConfig.SOCKET_CONNECTION_SETUP_TIMEOUT_MAX_MS_CONFIG,
      ProducerConfig.SOCKET_CONNECTION_SETUP_TIMEOUT_MS_CONFIG,
      ProducerConfig.TRANSACTIONAL_ID_CONFIG,
      ProducerConfig.TRANSACTION_TIMEOUT_CONFIG
    )
      .map(p => (p, p.replaceAll(raw"\.", "_").toUpperCase))
      .collect {
        case p if sys.env.contains(p._2) => (p._1, p._2)
      }
      .foreach(p => {
        logger
          .debug(s"Setting Kafka producer property ${p._1} -> ${sys.env(p._2)}")
        properties.setProperty(p._1, sys.env(p._2))
      })
    properties
  }
}
