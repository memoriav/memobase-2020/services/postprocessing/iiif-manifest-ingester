/*
 * IIIF Manifest Ingester
 * Writes IIIF manifests to database
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.scala.Logging
import org.mariadb.jdbc.MariaDbPoolDataSource

import java.sql.{Connection, PreparedStatement}
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

trait MariadbIndexer {
  self: Logging & AppSettings =>

  private val pool = {
    val p = new MariaDbPoolDataSource(mariaDBDSN)
    p.setUser(mariaDBUser)
    p.setPassword(mariaDBPassword)
    p
  }

  var connection: Connection = getConnection

  private def getConnection = {
    val c = pool.getConnection
    c.setAutoCommit(false)
    c
  }

  lazy private val preparedStatement = connection.prepareStatement(
    s"REPLACE INTO $mariaDBTable (sig, manifest) VALUES (?, ?)"
  )
  private val batchedRecords: mutable.Buffer[(String, Headers)] =
    mutable.Buffer()

  def writeToDB(
      id: String,
      iiifManifest: String,
      headers: Headers
  ): List[(String, Headers, Try[Int])] = {
    preparedStatement.setString(1, id)
    preparedStatement.setString(2, iiifManifest)
    logger.debug(s"Preparing record $id for indexing")
    batchedRecords.append((id, headers))
    preparedStatement.addBatch()
    if (batchedRecords.length >= 100) {
      flush()
    } else {
      List()
    }
  }

  def writeRemainingRecordsToDB(): List[(String, Headers, Try[Int])] = {
    if (batchedRecords.nonEmpty) {
      logger.debug(
        s"Write remaining ${batchedRecords.length} records to database"
      )
      flush()
    } else {
      List()
    }
  }

  def checkDBConnection(timeout: Int): Unit = {
    if (connection == null || !connection.isValid(timeout)) {
      if (!connection.isClosed) {
        connection.close()
      }
      connection = getConnection
    }
  }

  private def flush(): List[(String, Headers, Try[Int])] = {
    val res = executeBatch(preparedStatement)
    preparedStatement.clearBatch()
    val convertedRes = convertInsertResult(res)
    batchedRecords.clear()
    convertedRes
  }

  private def executeBatch(
      preparedStatement: PreparedStatement
  ): Try[Array[Int]] = {
    Try {
      logger.debug("Indexing records in database")
      val res = preparedStatement.executeBatch
      connection.commit()
      res
    } orElse {
      logger.warn("MariaDB connection error. Retrying after 10 seconds")
      Thread.sleep(10000)
      executeBatch(preparedStatement)
    }
  }

  private def convertInsertResult(
      res: Try[Array[Int]]
  ): List[(String, Headers, Try[Int])] = {
    res match {
      case Success(list) =>
        batchedRecords
          .zip(list)
          .map(e => (e._1._1, e._1._2, Success(e._2)))
          .toList
      case Failure(ex) =>
        batchedRecords.map(r => (r._1, r._2, Failure(ex))).toList
    }
  }

  def dbConnectorClose(): List[(String, Headers, Try[Int])] = {
    val res = executeBatch(preparedStatement)
    logger.debug("Closing connection to database")
    connection.close()
    pool.close()
    convertInsertResult(res)
  }

}
