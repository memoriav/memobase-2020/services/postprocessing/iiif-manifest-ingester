/*
 * IIIF Manifest Ingester
 * Writes IIIF manifests to database
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models.{
  ProcessingFatal,
  ProcessingStatus,
  ProcessingSuccess,
  Report
}
import org.apache.kafka.clients.producer.{
  KafkaProducer,
  ProducerRecord,
  RecordMetadata
}
import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.scala.Logging

import java.util.concurrent.{Future => JavaFuture}
import scala.util.{Failure, Success, Try}

abstract class Reporter {
  self: AppSettings & Logging =>

  private lazy val producer = Try(
    new KafkaProducer[String, String](producerProps)
  )

  def reportResult(res: (String, Headers, Try[Int])): Unit = {
    val id = s"https://memobase.ch/record/${res._1}"
    res._3 match {
      case Success(_) =>
        send(id, ProcessingSuccess, "Processing successful", res._2)
      case Failure(ex) =>
        send(
          id,
          ProcessingFatal,
          s"Processing failed: ${ex.getMessage}",
          res._2
        )
    }
  }

  private def send(
      id: String,
      status: ProcessingStatus,
      msg: String,
      headers: Headers
  ): JavaFuture[RecordMetadata] = {
    val report = Report(id, status, msg, reportingStepName)
    logger.debug(s"Sending report for record $id with status $status: $msg")
    if (producer.isFailure) {
      val failedProducer = producer.failed.get
      logger.error(failedProducer.getMessage)
      logger.debug("Caused by: " + failedProducer.getCause.getMessage)
      failedProducer
        .getStackTrace()
        .toList
        .foreach(ste => logger.debug(ste.toString))
    }
    producer.get.send(
      new ProducerRecord(
        kafkaReportingTopic,
        null,
        id,
        report.toString,
        headers
      )
    )
  }

  def closeReporter(): Unit = {
    logger.debug("Closing Kafka producer instance")
    if (producer.isFailure) {
      val failedProducer = producer.failed.get
      logger.error(failedProducer.getMessage)
      logger.debug("Caused by: " + failedProducer.getCause.getMessage)
      failedProducer
        .getStackTrace()
        .toList
        .foreach(ste => logger.debug(ste.toString))
    }
    producer.get.close()
  }
}
